import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoopbackService } from '../../service/loopback.service';
import { Router } from '@angular/router';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  s: any = [
    { nome: '', ate: '', de: '', marca: '' }
  ];
  arr: any;
  fmSearch: FormGroup;
  url: string;
  anos: any;
  marca: any;
  minValue = 0;
  maxValue = 90000;
  options: Options = {
    floor: 0,
    ceil: 100000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min Preço:</b> $' + value;
        case LabelType.High:
          return '<b>Max Preço:</b> $' + value;
        default:
          return '$' + value;
      }
    }
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private lb: LoopbackService
    ) { }

  ngOnInit() {
    this.getMarca();
    this.buildForm();
  }

  getMarca() {
    this.lb.getTable('marcas')
    .subscribe(data => this.marca = data);
  }

  onSearch() {
    this.arr = {
      marca: this.fmSearch.value.marca,
      de: this.fmSearch.value.de,
      ate: this.fmSearch.value.ate,
      modelo: this.fmSearch.value.modelo,
      minValue: this.minValue,
      maxValue: this.maxValue,
    };
    this.lb.updatedSelection(this.arr);
    this.router.navigate(['estoque']);
  }

  buildForm() {
    this.fmSearch = this.fb.group({
      marca: [''],
      de: [''],
      ate: [''],
      modelo: ['']
    });
  }
}

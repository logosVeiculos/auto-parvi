import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DicasComponent } from './dicas.component';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('DicasComponent', () => {
  let component: DicasComponent;
  let fixture: ComponentFixture<DicasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicasComponent ],
      imports: [HttpClientModule],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

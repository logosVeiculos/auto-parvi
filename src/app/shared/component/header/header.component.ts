import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  flag: boolean;
  bdisplay: string;
  cdisplay: string;
  icon: string;
  formWhatsApp: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.flag = true;
    this.bdisplay = 'block';
    this.cdisplay = 'none';
    this.icon = 'bars';
    this.buildFormDados();
  }


  buildFormDados() {
    this.formWhatsApp = this.fb.group({
      nome: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      telefone: ['', [Validators.minLength(11), Validators.maxLength(11), Validators.required]],
    });
  }

  onSend() {
    const url = 'https://wa.me/5211234567890';
    if (!this.formWhatsApp.valid) {
      return;
    }
    const nome = this.formWhatsApp.value.nome;
    // const nome = 'Hector Velasquez';
    console.log(nome.replace(/ /g, '%2E'));
    const email = this.formWhatsApp.value.email;
    const telefone = this.formWhatsApp.value.telefone;
    const link = `${url}?text=Nome%3A%20${nome.replace(/ /g, '%20')}%2E%20Email%3A%20${email}%2E%20Telefone%3A%20${telefone}%2E`;
    const form = this.formWhatsApp.value;
    window.open(link, '_blank');
  }
}

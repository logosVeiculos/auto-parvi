import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RdStationService } from '../../service/rd-station.service';

@Component({
  selector: 'app-simula',
  templateUrl: './simula.component.html',
  styleUrls: ['./simula.component.scss']
})
export class SimulaComponent implements OnInit {

  @Input() carro: number;
  sim: any;

  fmProposta: FormGroup;

  constructor(
    private lb: LoopbackService,
    private fb: FormBuilder,
    private rd: RdStationService
  ) {
    this.fmProposta = this.fb.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      telefone: ['', Validators.required],
      veiculo: ['', Validators.required],
      valor: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.onSimula(this.carro);
  }

  onSimula(id) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(data => {
      this.sim = data[0];
      const dados = `${data[0].marcaCarro} ${data[0].modeloCarro}`;
      // tslint:disable-next-line:no-string-literal
      this.fmProposta.controls['veiculo'].setValue(dados);
      // tslint:disable-next-line:no-string-literal
      this.fmProposta.controls['valor'].setValue(data[0].valor);
    });
  }

  onSubmit() {
    if (!this.fmProposta.valid) {
      return;
    }
    this.rd.sendProposta(this.fmProposta.value)
    .subscribe(() => {
      this.fmProposta.reset();
      Swal.fire(
        'Proposta enviada',
        'Em um momento entraremos em contato com você',
        'success'
      );
    });
  }

}

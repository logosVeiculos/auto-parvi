import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-complementares',
  templateUrl: './complementares.component.html',
  styleUrls: ['./complementares.component.scss']
})
export class ComplementaresComponent implements OnInit {

  @Input() carro: number;
  opcion: any;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onOpcion(this.carro);
  }

  onOpcion(id) {
    this.lb.getFindBy('complementos', 'carroId', id)
    .subscribe(data => this.opcion = data);
  }
}

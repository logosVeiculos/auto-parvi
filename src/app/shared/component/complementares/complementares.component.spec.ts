import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplementaresComponent } from './complementares.component';

describe('ComplementaresComponent', () => {
  let component: ComplementaresComponent;
  let fixture: ComponentFixture<ComplementaresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplementaresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplementaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

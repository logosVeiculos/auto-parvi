import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RdStationService } from '../../service/rd-station.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  fmNews: FormGroup;
  constructor(private rd: RdStationService, private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.fmNews = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  onNews() {
    if (!this.fmNews.valid) {
      return;
    }
    this.rd.sendNews(this.fmNews.value)
    .subscribe(data => {
      this.fmNews.reset();
      Swal.fire(
        'Ok',
        'Você começará a receber notícias de nós',
        'success'
      );
    });
  }

}

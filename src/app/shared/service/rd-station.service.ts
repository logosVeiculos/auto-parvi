import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RdStationService {

  config: any;
  constructor(private http: HttpClient) {
    this.config = {
      token: '55ace6101e241ec0f33ed17b24cea32d',
      url: 'https://www.rdstation.com.br/api/1.3/conversions'
    };
  }

  sendProposta(items: any) {
    items = {
      token_rdstation: this.config.token,
      identificador: 'Site - Simula',
      Nome: items.nome,
      email: items.email,
      Telefone: items.telefone,
      Veiculo: items.veiculo,
      Valor: items.valor
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }

  sendNews(items: any) {
    items = {
      token_rdstation: this.config.token,
      identificador: 'Site - Newsletter',
      email: items.email,
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }

  sendCotact(items: any) {
    items = {
      token_rdstation: this.config.token,
      identificador: 'Site - Fale Conosco',
      Nome: items.nome,
      Telefone: items.telefone,
      email: items.email,
      Mensagem: items.message
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }
}

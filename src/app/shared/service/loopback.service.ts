import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Search } from './search';

@Injectable({
  providedIn: 'root'
})

export class LoopbackService {

  url: string;
  public search: Search;
  private dataSource = new BehaviorSubject(this.search);
  data = this.dataSource.asObservable();

  constructor(private http: HttpClient) {
    this.url = 'https://api.autoparvioutlet.com.br/api';
  }

  /**
   * Select All
   * @param itens tabela
   * @param limit limite
   */
  getTable(itens, limit = 0) {
    if (limit === 0) {
      return this.http.get(`${this.url}/${itens}`);
    } else {
      return this.http.get(`${this.url}/${itens}?filter[limit]=${limit}`);
    }
  }

  getCount(item) {
    return this.http.get(`${this.url}/${item}/count`);
  }

  getFindById(table, iten) {
    return this.http.get(`${this.url}/${table}/${iten}`,
    { headers:
      { 'Access-Control-Allow-Origin': '*' }
    }
    );
  }

  getFindBy(table, field, search, limit= 0, order= 'ASC') {
    if (limit === 0) {
      return this.http.get(`${this.url}/${table}?filter[where][${field}]=${search}&filter[order]=id ${order}`);
    } else {
      return this.http.get(`${this.url}/${table}?filter[where][${field}]=${search}&[filter][limit]=${limit}&filter[order]=id ${order}`);
    }
  }

  getCountFindBy(item, field, search) {
    return this.http.get(`${this.url}/${item}/count?[where][${field}]=${search}`);
  }

  getFindBy2(table: string, field1: string, search1: string, field2: string, search2: string, limit= 0, order= 'ASC') {
    if (limit === 0) {
      // tslint:disable-next-line:max-line-length
      return this.http.get(`${this.url}/${table}?filter[where][${field1}]=${search1}&filter[where][${field2}]=${search2}&filter[order]=id ${order}`);
    } else {
      const A = `filter[where][${field1}]=${search1}`;
      const B = `filter[where][${field2}]=${search2}`;
      const sql = `${this.url}/${table}?${A}&${B}&[filter][limit]=${limit}&filter[order]=id ${order}`;
      console.log(sql);
      return this.http.get(sql);
    }
  }

  getCountFindBy2(table, field1, search1, field2, search2, limit= 0) {
    if (limit === 0) {
      return this.http.get(`${this.url}/${table}/count?[where][${field1}]=${search1}&[where][${field2}]=${search2}`);
    } else {
      const A = `filter[where][${field1}]=${search1}`;
      const B = `[where][${field2}]=${search2}`;
      return this.http.get(`${this.url}/${table}?${A}&${B}&[filter][limit]=${limit}`);
    }
  }

  getOfertas(ofertas: number, limit = 1) {
    const link = `${this.url}/carros?filter[where][ofertas]=${ofertas}&filter[where][status]=disponivel&filter[limit]=${limit}`;
    console.log('Link => ', link);
    return this.http.get(link);
  }

  getSearch(marca, de, ate, modelo) {
    let ans: any;
    let ans2: any;
    let ans3: any;
    if (de && ate) {
      ans = `&filter[where][ano][between][0]=${ de }&filter[where][ano][between][1]=${ ate }`;
    } else {
      ans = '';
    }
    if (modelo) {
      ans2 = `&filter[where][modeloCarro][like]=%${ modelo }%`;
    } else {
      ans2 = '';
    }
    if (marca) {
      ans3 = `&filter[where][marcaCarro]=${ marca }`;
    } else {
      ans3 = '';
    }
    const sql = `${this.url}/carros?filter[where][status]=disponivel${ ans }${ ans2 }${ ans3 }`;
    console.log(`${this.url}/carros?filter[where][status]=disponivel${ ans }${ ans2 }${ ans3 }`);
    return this.http.get(sql);
  }


  getEstoque(marca, cambio, combustivel, portas, ano) {
    let ans1: any;
    let ans2: any;
    let ans3: any;
    let ans4: any;
    let ans5: any;
    if (cambio) {
      ans1 = `&filter[where][transmissao]=${ cambio }`;
    } else {
      ans1 = '';
    }
    if (combustivel) {
      ans2 = `&filter[where][combustivel]=${ combustivel }`;
    } else {
      ans2 = '';
    }
    if (marca) {
      ans3 = `&filter[where][marcaCarro]=${ marca }`;
    } else {
      ans3 = '';
    }
    if (portas) {
      ans4 = `&filter[where][portas]=${ portas }`;
    } else {
      ans4 = '';
    }
    if (ano) {
      ans5 = `&filter[where][ano]=${ ano }`;
    } else {
      ans5 = '';
    }
    const sql = `${this.url}/carros?filter[where][status]=disponivel${ ans1 }${ ans2 }${ ans3 }${ ans4 }${ ans5 }`;
    console.log(`${this.url}/carros?filter[where][status]=disponivel${ ans1 }${ ans2 }${ ans3 }${ ans4 }${ ans5 }`);
    return this.http.get(sql);
  }

  /*** Query Executions */

  /**
   * Add Dados
   * @param item tabela
   * @param data array
   */
  postData(item, data) {
    return this.http.post(`${this.url}/${item}`, data, {headers:
      { 'Content-Type': 'application/json; charset=utf-8' }});
  }

  /**
   * Updade Dados
   * @param item tabela
   * @param data array
   */
  updateData(item, data) {
    return this.http.put(`${this.url}/${item}`, data, {headers:
      { 'Content-Type': 'application/json; charset=utf-8' }});
  }

  /**
   * Delete Dados
   * @param item tabela
   * @param id  ID
   */
  deleteData(item, id) {
    return this.http.delete(`${this.url}/${item}/${id}`);
  }


  /*** Behavior */
  /**
   * Graba Dados na Memoria
   * @param data array
   */
  updatedSelection(data: Search) {
    this.dataSource.next(data);
  }

  /** Connetc Blog */
  getPosts(id: number) {
    return this.http.get(`https://compranalogos.com.br/dicas/wp-json/wp/v2/posts?per_page=${id}`);
  }
}

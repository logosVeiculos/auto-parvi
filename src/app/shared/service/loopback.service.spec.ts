import { TestBed } from '@angular/core/testing';
import { LoopbackService } from './loopback.service';
import { HttpClientModule } from '@angular/common/http';

describe('LoopbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [LoopbackService],
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: LoopbackService = TestBed.get(LoopbackService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';
import { RdStationService } from './rd-station.service';
import { HttpClientModule } from '@angular/common/http';

describe('RdStationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [RdStationService],
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: RdStationService = TestBed.get(RdStationService);
    expect(service).toBeTruthy();
  });
});

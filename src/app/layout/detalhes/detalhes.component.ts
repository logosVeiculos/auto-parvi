import { Component, OnInit } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.scss']
})
export class DetalhesComponent implements OnInit {

  id: number;
  car: any;
  estado: any;
  cidade: any;
  oferta: string;

  constructor(
    private lb: LoopbackService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
    });
  }

  ngOnInit() {
    this.onCarro(this.id);
    this.spinner.show();
    setTimeout(() => {
        this.spinner.hide();
    }, 1000);
  }

  onCarro(id) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(data => {
      this.car = data;
      this.getEstado(data[0].estado);
      this.getCidade(data[0].cidade);
      this.getOfertas(data[0].ofertas);
    });
  }

  private getEstado(id) {
    this.lb.getFindBy('estados', 'id', id)
    .subscribe(data => this.estado = data[0].nome);
  }

  private getCidade(id) {
    this.lb.getFindBy('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0].nome);
  }

  private getOfertas(id) {
    this.lb.getFindBy('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0].nome);
  }
}
